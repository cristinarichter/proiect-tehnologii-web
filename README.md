# Proiect Tehnologii Web

## Student: Richter Cristina, anul 3 ID, Informatica-Economica

[PROIECT INITIAL]  
Pasi cand am facut proiectul pe c9.io:  
1. folosesc c9.io    
2. nvm install stable    
3. cd server (ma pozitionez in fisierul server)  
4. npm install (ca sa instalez dependintele)  
5. apare in proiect folder-ul node_modules  
6. instalez baza de date mysql cu comanda    
      
	  mysql-ctl start  

7. am primit credentialele de mai jos, pe care le-am folosit in fisierul server.js la linia   
```
      const sequelize = new Sequelize ('c9', 'cristinarichter')  
```
Root User: cristinarichter  
Database Name: c9. 
8. dupa ce s-a instalat baza de date, am oprit serverul de baze de date cu comanda:
```     
	mysql-ctl stop  
```     
9. apoi am pornit servesul cu comanda:  
```      
	node server.js  
```     
[PROIECTUL PREDAT]

1) Am instalat pe local node.js si npm [link](https://nodejs.org/en), am instalat versiunea LTS.  
2) Instalez pachetul npm create-react-app din
```
terminal (mac) 
command line (pc)
```
cu comanda: 
```
	npm install -g create-react-app
	// -g global (for all users)
```
3) creez un folder 
```
	create-react-app react-proiect
```
4) ma pozitionez in folder-ul nou creat
```
	cd react-app
```
5) pornesc npm, cu comanda de mai jos va porni un server local
```
	npm start
```
6) mi se deschide automat o pagina in browser-ul web implicit (in cazul meu Safari)
```
	localhost:3000 // se deschide automat o pagina, care nu contine in mod evident nimic, pentru ca nu am scris cod pentru aplicatia mea
```
7) acum in folder-ul react-proiect a fost generat cod in mod automat
```
	codul a fost generat cu JSX (care este un javascript extension) si care ne permite sa amestecam javascript cu html.

```
8) Component
```
	este o clasa javascript care optional primeste proprietati (numite props) si care returneaza un element react care descrie cum va aparea o sectiune de UI (user interface)
	au internal state si external props (properties)
	poate fi folosit chiar si in mai multe proiecte
	este de fapt o logica de contained view
```
9) state
```
	State se afla in interiorul unei componente si contine informatii interne despre component, ce vor putea fi schimbate in mod dinamic
	In cele mai multe cazuri va avea o stare initiala
	Vom schimba state in loc sa o modificam in mod direct cu: 
	
	
	this.setState()
	
	
	
```
10) orice Component trebuie sa aiba o metoda numita render()
```
	este apelata de fiecare data cand se schimba State
	aici se poate decide ce va vedea user-ul in functie de state
	chiar daca de fiecare data cand se modifica state apelam render() nu totul este "rerendered" pe pagina.
	acest lucru se intampla datorita lui virtula DOM - cu ajutorul caruia se gaseste cea mai eficienta metoda de a "render" doar ce s-a modificat
```	